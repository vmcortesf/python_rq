import time

import redis
from flask import Flask, request
from rq import Queue

from tasks import background_task

app = Flask(__name__)

r = redis.Redis(host="redis", port=6379, db=0)
q = Queue(connection=r)


@app.route("/task")
def add_task():

    requested_data = request.args.get("n")

    if requested_data:

        job = q.enqueue(background_task, requested_data)

        return f"Task {job.id} added to queue at {job.enqueued_at}. {len(q)} tasks in the queue"

    return "No value for n"


if __name__ == "__main__":
    app.run(debug=True)