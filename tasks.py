import time

def background_task(n):
    delay = 2

    print("Task runnning!")
    print(f"Simulating {delay} seconds delay")

    time.sleep(delay)

    print(len(n))
    print("Task Complete")

    return len(n)