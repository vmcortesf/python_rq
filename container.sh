APP_NAME="tutorial-redis"

docker run \
    --name=$APP_NAME \
    --publish="6379:6379" \
    --rm redis