FROM python:3.9-alpine

LABEL mantainer="Víctor M. Cortés Figueroa <vcortes@bildus.tech>"

# We don't want to run our application as root if it is not strictly necessary, even in a container.
# Create a user and a group called 'app' to run the processes.
# A system user is sufficient and we do not need a home.
# RUN adduser --system --group --no-create-home app

# Datetime to locale
RUN apk add tzdata && \
    cp /usr/share/zoneinfo/America/Monterrey /etc/localtime

COPY . /app
WORKDIR /app

RUN pip install -U pip && \
    pip install -r requirements.txt

